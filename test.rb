require 'minitest/autorun'
require 'sqlite3'
require_relative 'challenge'

class TestChallenge < MiniTest::Unit::TestCase
  # Se usa el metodo sql del archivo challenge.rb
  @@db = SQLite3::Database.open 'challenge.db'
  @@results = @@db.execute(sql)
  def test_size
    assert_equal(47, @@results.size)
  end

  def test_sum
    sum = @@results.map{ |r| r[3] }.reduce(:+)
    assert_equal(2195, sum)
  end

  def test_order
    order = @@results.map{ |r| r[3] }
    assert_equal(93, order.first + order.last)
  end

  def test_columns
    results = @@db.execute2(sql)
    keys = %w(identificacion titulo autor cantidad)
    assert_equal(keys, results.first, 'Cambia los nombre de las columnas')
  end
end